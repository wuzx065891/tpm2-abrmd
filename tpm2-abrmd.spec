%global selinuxtype targeted

Name:            tpm2-abrmd
Version:         3.0.0
Release:         2
Summary:         A system daemon implementing the TPM2 access broker (TAB) & Resource Manager (RM) spec from the TCG
License:         BSD
URL:             https://github.com/tpm2-software/tpm2-abrmd
Source0:         https://github.com/tpm2-software/tpm2-abrmd/releases/download/%{version}/%{name}-%{version}.tar.gz

Patch3001:       Hygon-Add-support-for-TCM-devices.patch

BuildRequires:   systemd pkgconfig(cmocka) pkgconfig(dbus-1) pkgconfig(gio-unix-2.0) pkgconfig(tss2-mu) pkgconfig(tss2-sys)
BuildRequires:   tpm2-tss-devel >= 2.4.0 libtool autoconf-archive libgcrypt libgcrypt-devel
BuildRequires:   chrpath
BuildRequires:   selinux-policy-devel pkgconfig(systemd)

# tpm2-abrmd depends on the package that contains itsSELinux policy module
Requires: (%{name}-selinux >= 2.3.3-2 if selinux-policy)

%description
This is a system daemon implementing the TPM2 access broker (TAB) & Resource Manager (RM) spec from the TCG. 
The daemon (tpm2-abrmd) is implemented using Glib and the GObject system. 

%package devel
Summary:         Files necessary to develop applications with tpm2-abrmd
Requires:        %{name}%{_isa} = %{version}-%{release}
Requires:        tpm2-tss-devel >= 2.4.0


%description devel
This package contains development library files, headers and config files of tpm2-abrmd

%package_help

%package selinux
BuildArch: noarch
Summary: SELinux policies for tpm2-abrmd
Requires: selinux-policy >= 0.0.1
Requires(post): selinux-policy-%{selinuxtype}
Requires(post): libselinux-utils
Requires(post): policycoreutils
Requires(post): policycoreutils-python-utils

%description selinux
This package contains SELinux policies for tpm2-abrmd

%prep
%autosetup -p1 -n %{name}-%{version}
autoreconf --install

%build
%configure --disable-static --disable-silent-rules \
           --with-systemdsystemunitdir=%{_unitdir} \
           --with-systemdpresetdir=%{_presetdir} \
           --with-sepolicy \
           --with-dbuspolicydir=%{_datadir}/dbus-1/system.d/ \
           --enable-unit

%make_build

%install
pushd selinux
install -d %{buildroot}%{_datadir}/selinux/packages
install -d -p %{buildroot}%{_datadir}/selinux/devel/include/contrib
install -p -m 644 tabrmd.if %{buildroot}%{_datadir}/selinux/devel/include/contrib
install -m 0644 tabrmd.pp.bz2 %{buildroot}%{_datadir}/selinux/packages
popd

%make_install
%delete_la_and_a

#Remove Rpath
chrpath -d $RPM_BUILD_ROOT%{_libdir}/libtss2-tcti-tabrmd.so.0.0.0
chrpath -d $RPM_BUILD_ROOT%{_sbindir}/tpm2-abrmd
mkdir -p $RPM_BUILD_ROOT/etc/ld.so.conf.d
%ifarch sw_64
echo "/usr/lib/" > $RPM_BUILD_ROOT/etc/ld.so.conf.d/%{name}-%{_arch}.conf
%else
echo "/usr/lib64/" > $RPM_BUILD_ROOT/etc/ld.so.conf.d/%{name}-%{_arch}.conf
%endif

%check
make check

%pre selinux
%selinux_relabel_pre -s targeted

%preun
%systemd_preun tpm2-abrmd.service

%post
/sbin/ldconfig
%systemd_post tpm2-abrmd.service

%post selinux
%selinux_modules_install -s targeted %{_datadir}/selinux/packages/tabrmd.pp.bz2

%postun
/sbin/ldconfig
%systemd_postun tpm2-abrmd.service

%postun selinux
if [ $1 -eq 0 ]; then
%selinux_modules_uninstall -s targeted tabrmd
fi

%posttrans selinux
%selinux_relabel_post -s targeted

%files
%defattr(-,root,root)
%license LICENSE
%doc README.md CHANGELOG.md
%{_libdir}/libtss2-tcti-tabrmd.so.*
%{_sbindir}/tpm2-abrmd
%{_unitdir}/tpm2-abrmd.service
%{_presetdir}/tpm2-abrmd.preset
%{_datadir}/dbus-1/system-services/com.intel.tss2.Tabrmd.service
%{_datadir}/dbus-1/system.d/tpm2-abrmd.conf
%config(noreplace) /etc/ld.so.conf.d/*

%files devel
%defattr(-,root,root)
%{_includedir}/tss2/tss2-tcti-tabrmd.h
%{_libdir}/libtss2-tcti-tabrmd.so
%{_libdir}/pkgconfig/tss2-tcti-tabrmd.pc

%files help
%{_mandir}/man3/*.3.gz
%{_mandir}/man7/*.7.gz
%{_mandir}/man8/*.8.gz

%files selinux
%defattr(-,root,root)
%license LICENSE
%{_datadir}/selinux/devel/include/contrib/tabrmd.if
%{_datadir}/selinux/packages/tabrmd.pp.bz2

%changelog
* Fri Aug 2 2024 chench <chench@hygon.cn> - 3.0.0-2
- Type:enhancement
- ID:NA
- SUG:NA
- DESC: add support for TCM devices

* Tue Jul 18 2023 jinlun<jinlun@huawei.com> - 3.0.0-1
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:update to 3.0.0

* Tue Jan 10 2023 jinlun <jinlun@huawei.com> - 2.4.1-2
- Type:enhancement
- ID:NA
- SUG:NA
- DESC: add code check in tpm2-abrmd

* Wed Nov 23 2022 jinlun<jinlun@huawei.com> - 2.4.1-1
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:update to 2.4.1

* Thu Nov 3 wuzx<wuzx1226@qq.com> - 2.4.0-3
- Type:feature
- CVE:NA
- SUG:NA
- DESC:change lib64 to lib when in sw64 architecture

* Mon Oct 10 2022 jinlun<jinlun@huawei.com> - 2.4.0-2
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:fix the problem that pointers access invalid addresses

* Tue Dec 14 2021 panxiaohe<panxiaohe@huawei.com> - 2.4.0-1
- Type:enhancement
- ID:NA
- SUG:NA
- DESC: update to 2.4.0

* Tue May 25 2021 Hugel<gengqihu1@huawei.com> - 2.3.3-2
- Type:bugfix
- ID:NA
- SUG:NA
- DESC: fix the SELinux label missing issue

* Mon Aug 31 2020 Hugel<gengqihu1@huawei.com> - 2.3.3-1
- Type:enhancement
- ID:NA
- SUG:NA
- DESC: update to 2.3.3

* Thu Jul 30 2020 linwei<linwei54@huawei.com> - 2.3.2-1
- Type:enhancement
- ID:NA
- SUG:NA
- DESC: update tpm2-abrmd to 2.3.2

* Fri Apr 24 2020 wanghongzhe<wanghongzhe@huawei.com> - 2.2.0-1
- Type:enhancement
- ID:NA
- SUG:NA
- DESC: update to 2.2.0

* Wed Mar 25 2020 wanghongzhe<wanghongzhe@huawei.com> - 2.0.1-4.h2
- Type:enhancement
- ID:NA
- SUG:NA
- DESC: fix communication faild to tpm because updated glib2

* Sat Mar 7 2020 wanghongzhe<wanghongzhe@huawei.com> - 2.0.1-4.h1
- Type:enhancement
- ID:NA
- SUG:NA
- DESC: remove rpath and runpath of exec files and libraries

* Wed Jan 22 2020 openEuler Buildteam <buildteam@openeuler.org> - 2.0.1-4
- Add buildrequires of libgcrypt-devel

* Wed Jan 22 2020 openEuler Buildteam <buildteam@openeuler.org> - 2.0.1-3
- Type: enhancement
- ID: NA
- SUG: NA
- DESC:add buildrequires

* Fri Sep 20 2019 openEuler Buildteam <buildteam@openeuler.org> - 2.0.1-2
- Package init

